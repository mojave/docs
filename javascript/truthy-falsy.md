# truthy and falsy
Truth or falsy is what a non-boolean value becomes when it is coerced to a boolean,.

### “falsy” values in JavaScript is as follows:

"" (empty string)

0, -0, NaN (invalid number)

null, undefined

false


### "truthy" values
Any value that’s not on this “falsy” list is “truthy.” Here are some examples of those:

"hello"

42

true

[ ], [ 1, "2", 3 ] (arrays)

{ }, { a: 42 } (objects)

function foo() { .. } (functions)