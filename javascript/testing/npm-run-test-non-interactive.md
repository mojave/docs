By default, create-react-app sets up the 'react-scripts test' script to use --watch.  This forces the test run into an interactive state:

```console
 PASS  src/App.test.js
  ✓ renders without crashing (2ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.845s
Ran all test suites related to changed files.

Watch Usage
 › Press a to run all tests.
 › Press f to run only failed tests.
 › Press p to filter by a filename regex pattern.
 › Press t to filter by a test name regex pattern.
 › Press q to quit watch mode.
 › Press Enter to trigger a test run.
```

After running tests, jest waits for user input.

To run the tests and avoid the 'Watch Usage' prompt, you can update the test script command in the scripts section of package.json with `--watchAll=fasle`:

```json
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --env=jsdom --watchAll=false",
    "eject": "react-scripts eject"
  },
```

[Reference to discussion of this issue in create-react-app repo](https://github.com/facebook/create-react-app/issues/784)