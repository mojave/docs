# Simulating a change on an input or select

## input value change
#### the code
```javascript
    onChange = (event) => {
        this.setState(prevState => { 
            return {
                myInputName: event.target.value,
                myInputValue: event.target.value
            }
        );
    }

    <input
      type="text"
      name={this.state.myInputName}
      value={this.state.myInputValue}
      onChange={this.onChange}
    />
```

#### the test
```javascript
    expect(wrapper.state().appSummary.data.appSummary.description).toEqual('initial description');

    const inputTextBox = wrapper.find('#myInput');
    expect(inputTextBox).toHaveLength(1);

    // Using name, because that's used in the onChange, but specify whichever event.target values you require.
    const event = { target: { name: 'nameOfMyInput', value: 'change to the value goes here' } };
    inputTextBox.simulate('change', event);
    
    // assert whatever you need here...
```


## select value change
Selects are handled in a similar fashion, however the 'event' is a little different.

A select, with three options, will have an event.target looking something like (only showing the relevant properties):
```javascript
    target{
        0: { id:3, name:'monkeys'},
        1: { id:24, name:'chickens'},
        2: { id:7, name:'snails'},
        selectedIndex:1
    }
```

So, the select event to construct, to simulate selection of the second option in the list, will look like:

```javascript
    const event = { target: { selectedIndex: 1, 1: { id: 24, value:'chickens' } } };
```

#### the code
```javascript
      <form>
        <select
          id='mySelect'
          value={get(props, 'selected', undefined)}
          onChange={props.onChange}
        >
          <option>
            {'select'}
          </option>
          {props.optionList
            ? props.optionList.map(optionItem => {
              return (
                <option
                  key={optionItem.id}
                  id={optionItem.id}
                  value={optionItem.name}
                >
                  {optionItem.name}
                </option>
              );
            })
            : null}
        </select>
      </form>
```

#### the test
```javascript
    const event = { target: { selectedIndex: 1, 1: { id: 24, value:'chickens' } } };
    wrapper.find('#mySelect').simulate('change', event);
```
