## Test coverage in Create React App
Create React App comes with Jest, which has coverage capabilities.  

### Running coverage report
To run a coverage report:

```console
npm test -- --coverage
```

### Configuring coverage
Configure coverage via package.json.  E.g:

```json
  "jest": {
    "collectCoverageFrom": [
      "src/**/*.{js,jsx}",
      "!<rootDir>/node_modules/",
      "!<rootDir>/src/apis/"
    ],
    "coverageThreshold": {
      "global": {
        "branches": 90,
        "functions": 90,
        "lines": 90,
        "statements": 90
      }
    },
    "coverageReporters": ["text"]
```

For more details, see https://facebook.github.io/create-react-app/docs/running-tests#coverage-reporting