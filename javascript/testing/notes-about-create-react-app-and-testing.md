## test files location
create-react-app configures jest to look for tests in folders named `__tests__`, and for files named *test.js or *spec.js.

https://facebook.github.io/create-react-app/docs/running-tests#filename-conventions
