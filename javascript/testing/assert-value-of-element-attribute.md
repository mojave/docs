# Asserting the value of an element's attribute

## Enzyme
Example using a button, and checking for the disabled value.

`Find` the button using combination of the selector (e.g. #id) and the attribute/value pair:

```javascript
    expect(wrapper.find(‘#saveButton [disabled=true])).toHaveLength(1);
```