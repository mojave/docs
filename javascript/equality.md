There are four equality operators:
* ==
* ===
* !=
* !==

There are really only two, since != and !== are just the converse of == and ===.


`==` checks for value equality with coercion allowed

`===` checks for value equality without allowing coercion  ( === is often called “strict equality” )