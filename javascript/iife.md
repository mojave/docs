# Immediately Invoked Function Expressions (IIFEs)
A function which is defined an executed in the same statement:

```javascript
(function helloWorld(){    
    console.log( "Hello World!" );
})();
```

The parens `()` on the end are what executes the function.  The parens around the function definition are there just to tell JS not to treat this as a normal function expression.