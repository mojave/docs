# javascript closures
A closure is "a way to 'remember' and continue to access a function’s scope (its variables) even once the function has finished running."  

A clousure is a function defined within another function (the parent function), returned from the parent function, and which has access to the parent function's properties even after the parent function is closed.


#### Example (from safari book)
```javascript
function parentFunction(x) {
    // parameter `x` is an inner variable

    // inner function `theClosure()` uses `x`, so
    // it has a "closure" over it
    function theClosure(y) {
        return y + x;
    };

    return theClosure;
}
```

```javascript
// `myFunc` gets a reference to the inner `theClosure(..)`
// function with closure over the `x` parameter of
// the outer `parentFunction(..)`
var myFunc = parentFunction( 1 );

// `myFunc2` gets a reference to the inner `theClosure(..)`
// function with closure over the `x` parameter of
// the outer `parentFunction(..)`
var myFunc2 = parentFunction( 10 );

myFunc( 4 );       // 5  <-- 1 + 4
myFunc( 32 );      // 33 <-- 1 + 32

myFunc2( 17 );      // 27 <-- 10 + 17
```

## Using closures to create javascript modules
This is a common use of closures in javascript.

```javascript
function Task(){
    var hours;

    function updateTimeSpent(hrs) {
        hours = hrs;

        // do the rest of the update work...
    }

    var publicAPI = {
        timeSpent: updateTimeSpent
    };

    return publicAPI;
}

// create a `User` module instance
var homework = Task();

homework.timeSpent( 3 );
```

homework is a task.  Calling timeSpent() on homework, results in updateTimeSpent (inside Task) being called.  Everything except publicAPI is hidden from the outside world.

