1. don't need constructor.  state is already available in the parent `React.Component`.  so, can set initial state with:  `state = {}`

2. don't need `bind(this)` if you use arrow functions.  arrow functions will use the `this` of the parent block.

3. don't use a Class if you don't need to manage state.  they use more memory and are more work to maintain.
