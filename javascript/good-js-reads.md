## Specific Topics
* [Why Would You Use Node.js?](https://medium.com/the-node-js-collection/why-the-hell-would-you-use-node-js-4b053b94ab8e)
* [Node.js Case Studies (from Node)](https://nodejs.org/en/foundation/case-studies/)
* https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API
* 

## General Reads
* [Javascript Weekly](https://javascriptweekly.com/)
* [DailyJS](https://medium.com/dailyjs)