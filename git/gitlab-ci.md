## anchors, aliases, and hidden keys

```yaml

# the '.' indicates a hidden key.  the hidden key in this case is 'tf_init_script'
# the '&' indicates an anchor.  the anchor can be referenced many times in the pipeline.
.tf_init_script: & tf_init_script
  - !reference [.proxy, script]
  - cd ./terraform
  - terraform init -backend-config="token=$TFE_TOKEN"

.tf_runner_template: &tf_runner_config
  image: $CI_REGISTRY/...
  tags:
    - my_runner_tag

.tf_common_template: &tf_common_config
  variables:
      TFE_TOKEN: $TFE_TOKEN_TEST
      TF_WORKSPACE: test
  rules:
      - when: on_success
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH #e.g. master/main
        variables:
          TFE_TOKEN: $TFE_TOKEN_PROD
          TF_WORKSPACE: prod

# this job extends the tf_runner_config and tf_common_config, adding a stage and script to them
# this job also references the tf_init_script in its own script section
tf-validate
  stage: tf-validate
  <<: *tf_runner_config
  <<: *tf_common_config
  script:
    - *tf_init_script
    - terraform validate
```

## skipping jobs within a pipeline
Looks like we could skip jobs in our pipelines with a little extra logic.  In addition to "-o ci.skip", there is a "-o ci.variable=...".  So, we can skip undesirable jobs during development cycles with something like:

```yaml
stages:
  - build
  - test

.skip_check:
  rules:
    - if: $SKIP_STUFF  # value of SKIP_STUFF doesn't matter
      when: never      # when SKIP_STUFF is present, skip the jobs extending this template
    - when: on_success # otherwise run the job

test:
  stage: test
  image: $CI_REGISTRY/...
  script:
    - echo "SKIP_STUFF = ${SKIP_STUFF}
  tags:
    - my_tag
```

Example push:

`git push -o ci.variable="SKIP_STUFF=anything" origin my_branch`

The value for SKIP_TESTS doesn't matter.  The pipeline just checks for the existence of the variable to make its decision.

<br/><br/>
