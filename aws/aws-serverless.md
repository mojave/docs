# troubleshooting

## CORS
1. First, verify you have auth enabled on the API gateway method (e.g. I did not have wolog set on the POST to /programs and got CORS error)
2. Second, setup your API gateway resource and methods as follows:
   1. Resource should *not* have lambda proxy integration
   2. The method, e.g. POST, should have mapping templates set (see mappingTempatesApigw.png in this directory) on the integration request settings
3. Third, in your lambda, retrieve request body from `event['body-json']`
