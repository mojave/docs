

# lambda triggers
* API requests via API Gateway
* Cloudwatch scheduled events
* S3 file uploads
* DynamoDB Stream changes
* Direct invocations via CLI or SDK
* and more...