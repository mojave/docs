Doc project for various readme files.

> It is not the critic who counts; not the man who points out how the strong man stumbles<br/>
> or where the doer of deeds could have done them better. The credit belongs to the man who is actually in the arena,<br/>
> whose face is marred by dust and sweat and blood; who strives valiantly; who errs, who comes short again and again,<br/>
> because there is no effort without error and shortcoming.  _Theodore Roosevelt_