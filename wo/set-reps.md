From https://www.t-nation.com/training/set-rep-bible

## sets/reps


|Goal   |Parameter Options  |Total Reps |Loading    |Sessions Per Week| Notes|
|---|---|---|---|---|---|
| Strength |8 x 3,<br/> 10 x 3,<br/> 12 x 3, <br/>7 x 4, <br/>8 x 4, <br/>or 9 x 4| 24-36|  80-90% of 1RM|  2-4|    These would all work well to increase maximal strength and hypertrophy|
| Hypertrophy |6 x 6,<br/>4 x 12,<br/>or 5 x 10   |36-50  |70-80% of 1RM  |2-4    |These would provide a powerful hypertrophy effect with the prescribed loads.|
| Fat loss| 4 x 6,<br/>4 x 8,<br/>5 x 5,<br/>or 5 x 6   |24-36  |70-80% of 1RM  |2-3    |These will all work well to maintain muscle mass during hypocaloric eating phases.|