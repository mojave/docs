# Contents
* [Front End](https://gitlab.com/mojave/docs/blob/master/learning-path.md#front-end)
   * [Javascript](https://gitlab.com/mojave/docs/blob/master/learning-path.md#javascript)
     * [Testing](https://gitlab.com/mojave/docs/blob/master/learning-path.md#js-testing) 
        * [Jest]
        * [Mocha]
        * [Chai]
        * [Jasmine]
        * [Enzyme]
        * [Yadda]
        * [Selenium]
     * [Build Tools](https://gitlab.com/mojave/docs/blob/master/learning-path.md#js-build-tools) 
     * [Frameworks](https://gitlab.com/mojave/docs/blob/master/learning-path.md#js-front-end-frameworks) 
        * [React](https://gitlab.com/mojave/docs/blob/master/learning-path.md#react)



<br/><br/>
# Front End
## HTML


## CSS


## Javascript
### Javascript Fundamentals
* [Mozilla Developer Network - Beginner, Intermediate, and Advanced Tutorials](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* [Programming Javascript Applications](https://www.safaribooksonline.com/library/view/programming-javascript-applications)

### Js Testing
* Jest
* Mocha
* Chai
* Jasmine
* Enzyme


### Js Build Tools
* webpack

### Js Front-End Frameworks
#### React
* React
* Redux




