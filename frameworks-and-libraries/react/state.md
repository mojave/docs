## Where to store state in a component tree?
[React recommends](https://reactjs.org/docs/lifting-state-up.html): "lifting the shared state up to their closest common ancestor"
