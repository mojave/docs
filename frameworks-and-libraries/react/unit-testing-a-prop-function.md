## TO UNIT TEST A FUNCTION WHICH WAS PASSED AS A PROP

```javascript
import React from 'react';
import SomeComponent from 'project/src/components/Common/ConfirmationModal';
import { mount, shallow } from 'enzyme';
import { stub } from 'sinon';
import toJson from 'enzyme-to-json';

describe('ConfirmationModal', () => {
  let wrapper = null;
  let props = null;

  beforeEach(() => {
    props = {
      message: 'test message',
      handleConfirm: stub(),
      handleCancel: stub()
    };
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.unmount();
    }
  });

  it('renders', () => {
    wrapper = shallow(<ConfirmationModal {...props} />);
    let tree = toJson(wrapper);
    expect(tree).toMatchSnapshot();
  });

  describe('buttons', () => {
    it('clicking confirm calls props.handleConfirm()', () => {
      wrapper = mount(<ConfirmationModal {...props} />);
      wrapper.find('#buttonConfirm').simulate('click');
      expect(props.handleConfirm.calledOnce).toEqual(true);
    });

    it('clicking cancel calls props.handleCancel()', () => {
      wrapper = mount(<ConfirmationModal {...props} />);
      wrapper.find('#buttonCancel').simulate('click');
      expect(props.handleCancel.calledOnce).toEqual(true);
    });
  });
});
```