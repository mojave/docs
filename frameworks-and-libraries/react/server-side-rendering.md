# SSR - What, How, Why?

Isomorphic, Universal, and SSR - _all the same thing_

## Client vs Server Side Rendering

<img src="./client-vs-ssr.png" />

## Benefits of SSR

* **Faster load times**
   - the rendering is done by server, and not impeded by a poor performing cliient

* **Improved SEO**
   - search bots (like google's) see a server rendered app as HTML
   - however, they see client rendered apps as javascript

## What exactly is it?
Server renders the first page of the SPA in html, and delivers to the browser.  This saves some of the initial http/axios requests from the browser to build that initial page.

All of the bundle.js is still retrieved by the rendered index page when loaded in the browser, but the initial page load is faster, since the server already rendered it to html.
