[Good info from jest's docs](https://jestjs.io/docs/en/asynchronous).


## Unit testing a function which has async/promise logic in it.
In this example, we want to test `handleUpdate()`.  handleUpdate depends on retrieve(), which makes some axios call and returns a promise.
### Example code to be tested:
```javascript
  handleUpdate() {
    this.retrieve().then(data => {
      this.setState({
        data: data,
        showModal: true
      });
    });
  }
  
  retrieve() {
    return someAxiosCall();
  }
```

### How to test it
The key is the asycn/await...

```javascript
    it('handleUpdate', async () => {
      expect.assertions(1);
      const setStateSpy = jest.spyOn(MyReactComponent.prototype, "setState");
      let retrieveMock = jest.spyOn(MyReactComponent.prototype, "retrieve");
      retrieveMock.mockImplementation(() => {
        return Promise.resolve(['someData'])
      });
      
      let wrapper = mount(<MyReactComponent {...props} />);

      await wrapper.instance().handleUpdate();
      expect(setStateSpy).toHaveBeenCalledWith({
        data: ['someData'],
        showModal: true
      });

      setStateSpy.mockRestore();
      retrieveMock.mockRestore();
      wrapper.unmount();
    });
```