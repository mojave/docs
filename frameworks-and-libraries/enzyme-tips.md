## Finding the text/innerHTML of an html element in a mounted component
[From Enzyme docs](https://airbnb.io/enzyme/docs/api/ReactWrapper/text.html)

```javascript
  it('label matches what it's supposed to', () => {
    wrapper = mount(<MySuperComponent {...props} />);
    let labelText = wrapper.find(SomeChildComponent).find('label').text();
    expect(labelText).toEqual('Modify User');
  });
```
