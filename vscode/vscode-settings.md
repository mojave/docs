## Shortcut to switch between terminals
Add this to your keybindings.json (just do ctrl+p keybindings.json to open it):

```javascript
[
    {
        "key": "cmd+right",
        "command": "workbench.action.terminal.focusNext"
      },
      {
        "key": "cmd+left",
        "command": "workbench.action.terminal.focusPrevious"
      }
]
```

## Copy text on select in terminal
1. Go to settings (⌘,)
1. Search for terminal.integrated.copyOnSelection.
1. Check the checkmark.
