# CSS Rule Structure
![](./resources/structure-of-css-rule-sm.png)

## Selectors
Selectors identify to what the style will be applied.  Selectors can be:

* html elements
* css classes
* ids

```css
  table   { border: solid }
  .aClass { color:  blue }
  #anId   { font:   Tahoma }
```

## Grouping Selectors
Grouping selectors allows you to apply a single style to multiple selectors.

For example:

```css
 th, .myClass, #someId { font:'Courier New'}
```

Will apply the same font style to all of `th` elements, elements with `myClass` applied, and elements with id of `someId`.


### Universal Selector
Use an asterisk to apply a style to all elements:

```css
  * { color: gray }
```


### More About Class Selectors
A class selector can be applied to all elements with the class declared, or to only specific elements with the class declared.

The following rule will apply the color orange to all `p` tags which have the `citrus` class declared:

```css
 p.citrus { color: orange }
```

And, this one will apply the color yellow to all `h3` tags which have the `citrus` class declared:

```css
 h3.citrus { color: yellow }
```

And, both are declared in the same css file:
```css
p.citrus { color: orange }
h3.citrus { color: yellow }
```

In this example, we apply the color green to only those elements which have both the `lime` and `avocado` classes (the two classes can be in any order):

```css
.lime.avocado { color: green }
```

And, this one applies the same way, but only to `span` elements which have both classes:

```css
span.lime.avocado { color: green }
```

### Attribute Selectors
You can also apply styles to elements which have a particular attribute.  

The following example applies the color blue to any element which has an `href` attribute:

```css
 *[href] { color: blue }
```

And, this one applies the color purple to any `a` element which has both the `href` and `title` attributes:

```css
 a[href][title] { color: purple }
```

Further, we can apply a style to an element which has a specific value for a specific attribute:

```css
 a[href="http://www.w3schools.com"] { color: magenta }
```


## Keywords
In a CSS declaration, the 'value' is typically one or more 'keywords'.  If more than one keyword, it will be a space separated list.  

For example, in the following declaration, 'medium' and 'Consolas' are keywords for the value associated with the propery 'font'.

```css
 { font: medium Consolas }
```
 
