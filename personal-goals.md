# Personal Goals

Picked up this idea from [@cujarrett](https://github.com/cujarrett), and he was inspired by [this](http://una.im/personal-goals-guide).

## 2019

### Overarching Goals for 2019:
1. JavaScript - dive deeper
2. Gain first hand knowledge and expereince in several popular JavaScript single page application frameworks
3. Continue to teach
4. Understand how and why something works, rather than just how to do it.
5. Learn python basics


### Things I'll Do in 2019:

- [ ] [Complete kubernetes Tutorial & Read Docs](https://github.com/cujarrett/personal-goals/issues/4)
- [ ] [Cloud Native Certified Kubernetes Application Developer (CKAD) Program](https://github.com/cujarrett/certified-kubernetes-application-developer-course)
- [ ] Learn & Implement gitlab-ci-runner
- [ ] Learn about NGiNX](https://github.com/cujarrett/personal-goals/issues/16)
- [ ] Learn about JWT](https://github.com/cujarrett/personal-goals/issues/9)
- [ ] Build an Electron app](https://github.com/cujarrett/learning-electron)
- [ ] Learn Flexbox](https://github.com/cujarrett/personal-goals/issues/5)
- [ ] [Learn the PRPL Pattern](https://github.com/cujarrett/personal-goals/issues/14)
- [ ] Build Something with Hugo
- [ ] Build something cool with React JS
- [ ] Deploy an app to AWS
- [ ] Build something cool with Vue JS
- [ ] Build something cool with Angular JS
- [ ] Build something cool with Ember JS
- [ ] Learn React Hooks
- [ ] Read through all of Google's [Web Fundamentals](https://developers.google.com/web/fundamentals/)


## 2020

### Overarching Goals for 2020:
1. Learn more DevOps
2. Continue to teach
3. Form my own opinions rather than recite what someone else said/ wrote
4. Understand how and why something works, rather than just how to do it.

### Things I'll Do in 2020:

- [ ] ...