# npkill
npkill will delete node_modules from any projects under the present working directory.

```shell
$ cd dev/code

$ npkill
