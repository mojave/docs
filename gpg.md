# Encrypt with a symmetric key
This method will ask you to enter a passphrase which you will give to your receiver in order to decrypt the file

```bash
$ gpg -c file_sym
```


# Decrypt a symmetric encryption

### This is the method I've used successfully.
```bash
$ gpg -d file_sym.gpg
```

### These are other options
```bash
$ gpg --output file-content file_sym.gpg

$ gpg file_sym.gpg
```
