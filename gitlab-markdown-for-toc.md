To create a TOC for gitlab markdown file, you can reference any header:

[ThisHeaderWillAutomaticallyHaveALinkCreatedByGitlab](#thisheaderwillautomaticallyhavealinkcreatedbygitlab)

## ThisHeaderWillAutomaticallyHaveALinkCreatedByGitlab
The link to the above header will be in all lowercase.  So, you can reference it with:

`[ThisHeaderWillAutomaticallyHaveALinkCreatedByGitlab](#thisheaderwillautomaticallyhavealinkcreatedbygitlab)`


If you're unsure of the anchor, just click on the little link icon next to the header when in view mode.  (hover over the header, and the link icon will appear).